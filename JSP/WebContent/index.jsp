<!-- Directivas -->
<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<%@ page import="java.util.Calendar" %>

<%@ page errorPage="paginaError.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
		<title>Insert title here</title>
	</head>
	
	<!-- Declaraciones -->
	<%! Calendar calendario; %>
	
<body>
	
	<!-- Scriptlets -->
	<% calendario = Calendar.getInstance(); %>
	
	<!-- Expresiones -->
	<%= calendario.get(Calendar.HOUR_OF_DAY) %> :
	<%= calendario.get(Calendar.MINUTE) %> :
	<%= calendario.get(Calendar.SECOND) %> 
	
	<a href="tags.jsp">Tags</a>
	
</body>

</html>